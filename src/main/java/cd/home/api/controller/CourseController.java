package cd.home.api.controller;

import cd.home.api.entitiy.Course;
import cd.home.api.entitiy.Topic;
import cd.home.api.service.CourseService;
import cd.home.api.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;

    @RequestMapping("/")
    public Strin home(){return "<h1>Welcome to our api...</h1>";}

    @RequestMapping("/user/{user}")
    public String home1(@PathVariable String user){return "<h1>Welcome "+user+" to our api...</h1>";}

    @RequestMapping("/admin/{admin}")
    public String home2(@PathVariable String admin){return "<h1>Welcome "+admin+" to our api...</h1>";}

    @RequestMapping("/topics/{id}/courses")
    public List<Course> getAllCourses(@PathVariable String id) {
        return courseService.getAllCourses(id);
    }

    @RequestMapping("/topics/{topicId}/courses/{id}")
    public Optional<Course> getCourse(@PathVariable String id) {
        return courseService.getCourse(id);
    }

    @RequestMapping(value = "/topics/{topicId}/courses", method = RequestMethod.POST)
    public void addCourse(@RequestBody Course course,@PathVariable String topicId) {
        course.setTopic(new Topic(topicId,"",""));
        courseService.addCourse(course);
    }


    @RequestMapping(value = "/topics/{topicId}/courses/{id}", method = RequestMethod.PUT)
    public void updateCourse(@RequestBody Course course, @PathVariable String id,@PathVariable String topicId) {
        course.setTopic(new Topic(topicId,"",""));
        courseService.updateCourse(course);
    }

    @RequestMapping(value = "/topics/{topicId}/courses/{id}", method = RequestMethod.DELETE)
    public void deleteCourse(@PathVariable String id) {
        courseService.deleteCoourse(id);
    }

}
