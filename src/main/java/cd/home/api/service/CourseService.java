package cd.home.api.service;

import cd.home.api.entitiy.Course;
import cd.home.api.entitiy.Topic;
import cd.home.api.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseService {
    @Autowired
    private CourseRepository courseRepository;

    public List<Course> getAllCourses(String topicId) {
        return (List<Course>) courseRepository.findByTopicId(topicId);
    }

    public Optional<Course> getCourse(String id) {
        return courseRepository.findById(id);
    }

    public void addCourse(Course course) {
        courseRepository.save(course);
    }

    public void deleteCoourse(String id) {
        List<Course> courses = new ArrayList<Course>();
        courseRepository.findAll().forEach(courses::add);
        courseRepository.delete(courses.stream().filter(c -> c.getId().equals(id)).findFirst().get());
    }

    public void updateCourse(Course course) {
        courseRepository.save(course);
    }

}
