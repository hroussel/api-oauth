package cd.home.api.service;

import cd.home.api.entitiy.Topic;
import cd.home.api.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class TopicService {
    @Autowired
    private TopicRepository topicRepository;


    public void addTopics() {
        List<Topic> topics = new ArrayList<>(Arrays.asList(new Topic("spring", "spring framework", "spring Framework Desc"),
                new Topic("java", "java core", "Core Java Desc"),
                new Topic("javascript", "javascript", "javascript Desc"),
                new Topic("python", "python", "python Desc")));
        topicRepository.saveAll(topics);
    }


    public List<Topic> getAllTopics() {
        return (List<Topic>) topicRepository.findAll();
    }

    public Optional<Topic> getTopic(String id) {
//        return this.getAllTopics().stream().filter(t -> t.getId().equals(id)).findFirst().get();
        return topicRepository.findById(id);
    }

    public void addTopic(Topic topic) {
        topicRepository.save(topic);
    }

    public void deleteTopic(String id) {
        topicRepository.delete(this.getAllTopics().stream().filter(t -> t.getId().equals(id)).findFirst().get());
    }

    public void updateTopic(Topic top, String id) {
        topicRepository.save(top);
    }
}
